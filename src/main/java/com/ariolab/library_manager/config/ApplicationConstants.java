package com.ariolab.library_manager.config;

public class ApplicationConstants {
	// Regex for acceptable logins
	public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

	public static final String SYSTEM_ACCOUNT = "system";

	// Spring profiles for development, test and production
	public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
	public static final String SPRING_PROFILE_TEST = "test";
	public static final String SPRING_PROFILE_PRODUCTION = "prod";
	// Spring profile used to disable swagger
	public static final String SPRING_PROFILE_SWAGGER = "swagger";
	// Spring profile used to disable running liquibase
	public static final String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";

	private ApplicationConstants() {
	}
}
