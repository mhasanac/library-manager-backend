package com.ariolab.library_manager.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Properties specific to app backend.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

	private final CorsConfiguration cors = new CorsConfiguration();
	
	private final Security security = new Security();

	private final Swagger swagger = new Swagger();

	private final Logging logging = new Logging();

	public Security getSecurity() {
		return security;
	}

	public Swagger getSwagger() {
		return swagger;
	}

	public Logging getLogging() {
		return logging;
	}

	public CorsConfiguration getCors() {
		return cors;
	}

	

	public static class Security {

		private final ClientAuthorization clientAuthorization = new ClientAuthorization();

		private final Authentication authentication = new Authentication();

		private final RememberMe rememberMe = new RememberMe();

		public ClientAuthorization getClientAuthorization() {
			return clientAuthorization;
		}

		public Authentication getAuthentication() {
			return authentication;
		}

		public RememberMe getRememberMe() {
			return rememberMe;
		}

		public static class ClientAuthorization {

			private String accessTokenUri = null;

			private String tokenServiceId = null;

			private String clientId = null;

			private String clientSecret = null;

			public String getAccessTokenUri() {
				return accessTokenUri;
			}

			public void setAccessTokenUri(String accessTokenUri) {
				this.accessTokenUri = accessTokenUri;
			}

			public String getTokenServiceId() {
				return tokenServiceId;
			}

			public void setTokenServiceId(String tokenServiceId) {
				this.tokenServiceId = tokenServiceId;
			}

			public String getClientId() {
				return clientId;
			}

			public void setClientId(String clientId) {
				this.clientId = clientId;
			}

			public String getClientSecret() {
				return clientSecret;
			}

			public void setClientSecret(String clientSecret) {
				this.clientSecret = clientSecret;
			}
		}

		public static class Authentication {

			private final Jwt jwt = new Jwt();

			public Jwt getJwt() {
				return jwt;
			}

			public static class Jwt {

				private String secret = null;

				private long tokenValidityInSeconds = 1800; // 0.5 hour

				private long tokenValidityInSecondsForRememberMe = 2592000; // 30 hours;

				public String getSecret() {
					return secret;
				}

				public void setSecret(String secret) {
					this.secret = secret;
				}

				public long getTokenValidityInSeconds() {
					return tokenValidityInSeconds;
				}

				public void setTokenValidityInSeconds(long tokenValidityInSeconds) {
					this.tokenValidityInSeconds = tokenValidityInSeconds;
				}

				public long getTokenValidityInSecondsForRememberMe() {
					return tokenValidityInSecondsForRememberMe;
				}

				public void setTokenValidityInSecondsForRememberMe(long tokenValidityInSecondsForRememberMe) {
					this.tokenValidityInSecondsForRememberMe = tokenValidityInSecondsForRememberMe;
				}
			}
		}

		public static class RememberMe {

			@NotNull
			private String key = null;

			public String getKey() {
				return key;
			}

			public void setKey(String key) {
				this.key = key;
			}
		}
	}

	public static class Swagger {
		private String title = "Application API";

		private String description = "API documentation";

		private String version = "0.0.1";

		private String termsOfServiceUrl = null;

		private String contactName = null;

		private String contactUrl = null;

		private String contactEmail = null;

		private String license = null;

		private String licenseUrl = null;

		private String defaultIncludePattern = "/api/.*";

		private String host = null;

		private String[] protocols = {};

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getTermsOfServiceUrl() {
			return termsOfServiceUrl;
		}

		public void setTermsOfServiceUrl(String termsOfServiceUrl) {
			this.termsOfServiceUrl = termsOfServiceUrl;
		}

		public String getContactName() {
			return contactName;
		}

		public void setContactName(String contactName) {
			this.contactName = contactName;
		}

		public String getContactUrl() {
			return contactUrl;
		}

		public void setContactUrl(String contactUrl) {
			this.contactUrl = contactUrl;
		}

		public String getContactEmail() {
			return contactEmail;
		}

		public void setContactEmail(String contactEmail) {
			this.contactEmail = contactEmail;
		}

		public String getLicense() {
			return license;
		}

		public void setLicense(String license) {
			this.license = license;
		}

		public String getLicenseUrl() {
			return licenseUrl;
		}

		public void setLicenseUrl(String licenseUrl) {
			this.licenseUrl = licenseUrl;
		}

		public String getDefaultIncludePattern() {
			return defaultIncludePattern;
		}

		public void setDefaultIncludePattern(String defaultIncludePattern) {
			this.defaultIncludePattern = defaultIncludePattern;
		}

		public String getHost() {
			return host;
		}

		public void setHost(final String host) {
			this.host = host;
		}

		public String[] getProtocols() {
			return protocols;
		}

		public void setProtocols(final String[] protocols) {
			this.protocols = protocols;
		}
	}

	public static class Logging {

		private final Logstash logstash = new Logstash();

		public Logstash getLogstash() {
			return logstash;
		}

		public static class Logstash {

			private boolean enabled = false;

			private String host = "localhost";

			private int port = 5000;

			private int queueSize = 512;

			public boolean isEnabled() {
				return enabled;
			}

			public void setEnabled(boolean enabled) {
				this.enabled = enabled;
			}

			public String getHost() {
				return host;
			}

			public void setHost(String host) {
				this.host = host;
			}

			public int getPort() {
				return port;
			}

			public void setPort(int port) {
				this.port = port;
			}

			public int getQueueSize() {
				return queueSize;
			}

			public void setQueueSize(int queueSize) {
				this.queueSize = queueSize;
			}
		}
	}

}
