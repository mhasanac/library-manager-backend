package com.ariolab.library_manager.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ariolab.library_manager.domain.User;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findOneByEmailIgnoreCase(String email);

	@EntityGraph(attributePaths = "authorities")
	Optional<User> findOneWithAuthoritiesById(Long id);

	@EntityGraph(attributePaths = "authorities")
	Optional<User> findOneWithAuthoritiesByEmail(String email);

	@Query("SELECT DISTINCT user FROM User user JOIN user.authorities authority WHERE "+
			"(:email IS NULL OR LOWER(user.email) LIKE LOWER(CONCAT('%', :email, '%'))) AND "+
			"(:firstName IS NULL OR LOWER(user.firstName) LIKE LOWER(CONCAT('%', :firstName, '%'))) AND " +
			"(:lastName IS NULL OR LOWER(user.lastName) LIKE LOWER(CONCAT('%', :lastName, '%'))) AND " +
			"(:authorities IS NULL " + 
			"OR (authority.name IN (:authorities))" +
			")")
	Page<User> findAllByCustomQuery(Pageable pageable, String email, String firstName, String lastName, List<String> authorities);
}
