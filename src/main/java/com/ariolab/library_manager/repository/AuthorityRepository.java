package com.ariolab.library_manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ariolab.library_manager.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
