package com.ariolab.library_manager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ariolab.library_manager.domain.Book;
import com.ariolab.library_manager.domain.BookGenre;

/**
 * Spring Data JPA repository for the BookGenre entity.
 */
public interface BookGenreRepository extends JpaRepository<BookGenre, Long> {

	@Query("select distinct bookGenre from BookGenre bookGenre")
	List<Book> findAllWithEagerRelationships();

	@Query("select bookGenre from BookGenre bookGenre where bookGenre.id =:id")
	Book findOneWithEagerRelationships(@Param("id") Long id);

}
