package com.ariolab.library_manager.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ariolab.library_manager.domain.Book;

/**
 * Spring Data JPA repository for the Book entity.
 */
public interface BookRepository extends JpaRepository<Book, Long> {

	@Query("select distinct book from Book book")
	List<Book> findAllWithEagerRelationships();

	@Query("select book from Book book where book.id =:id")
	Book findOneWithEagerRelationships(@Param("id") Long id);
	
	@Query("SELECT DISTINCT book FROM Book book LEFT JOIN book.bookGenre bookGenre WHERE "+
			"(:name IS NULL OR LOWER(book.name) LIKE LOWER(CONCAT('%', :name, '%'))) AND "+
			"(:author IS NULL OR LOWER(book.author) LIKE LOWER(CONCAT('%', :author, '%'))) AND "+
			"(:publisher IS NULL OR LOWER(book.publisher) LIKE LOWER(CONCAT('%', :publisher, '%'))) AND " +
			"(:bookGenreId IS NULL OR book.bookGenre.id = :bookGenreId)")
	Page<Book> findAllByCustomQuery(Pageable pageable,  String name, String author, String publisher, Long bookGenreId);
}
