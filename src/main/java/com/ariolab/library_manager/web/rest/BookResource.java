package com.ariolab.library_manager.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ariolab.library_manager.security.AuthoritiesConstants;
import com.ariolab.library_manager.service.BookService;
import com.ariolab.library_manager.service.dto.BookDTO;
import com.ariolab.library_manager.service.dto.BookGenreDTO;
import com.ariolab.library_manager.web.util.HeaderUtil;
import com.ariolab.library_manager.web.util.PaginationUtil;
import com.ariolab.library_manager.web.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Book.
 */
@RestController
@RequestMapping("/api/books")
public class BookResource {

	private final Logger log = LoggerFactory.getLogger(BookResource.class);

	private static final String ENTITY_NAME = "Book";

	private final BookService bookService;

	public BookResource(BookService bookService) {
		this.bookService = bookService;
	}

	/**
	 * POST /books : Create a new Book.
	 *
	 * @param BookDTO the BookDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new BookDTO, or with status 400 (Bad Request) if the Book has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("")
	public ResponseEntity<BookDTO> createBook(@Valid @RequestBody BookDTO bookDTO) throws URISyntaxException {
		log.debug("REST request to save Book : {}", bookDTO);
		if (bookDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new Book cannot already have an ID")).body(null);
		}

		// set mandatory values
		bookDTO.setCreatedDate(Instant.now());
		bookDTO.setUpdatedDate(Instant.now());

		BookDTO result = bookService.save(bookDTO);
		return ResponseEntity.created(new URI("/api/books/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /books : Updates an existing Book.
	 *
	 * @param BookDTO the BookDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated BookDTO, or with status 400 (Bad Request) if the BookDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the BookDTO couldnt be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("")
	public ResponseEntity<BookDTO> updateBook(@Valid @RequestBody BookDTO BookDTO) throws URISyntaxException {
		log.debug("REST request to update Book : {}", BookDTO);
		if (BookDTO.getId() == null) {
			return createBook(BookDTO);
		}
		// set mandatory values
		BookDTO.setUpdatedDate(Instant.now());

		BookDTO result = bookService.save(BookDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, BookDTO.getId().toString())).body(result);
	}

	/**
	 * GET /books : get all the books.
	 *
	 * @param pageable the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of books in body
	 */
	@GetMapping("")
	public ResponseEntity<List<BookDTO>> getAllBooks(@ApiParam Pageable pageable, 
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String author,
			@RequestParam(required = false) String publisher,
			@RequestParam(required = false) Long bookGenre) {
		log.debug("REST request to get a page of books");
		Page<BookDTO> page = bookService.findAll(pageable, name, author, publisher, bookGenre);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/books");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /books/:id : get the "id" Book.
	 *
	 * @param id the id of the BookDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the BookDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/{id}")
	public ResponseEntity<BookDTO> getBook(@PathVariable Long id) {
		log.debug("REST request to get Book : {}", id);
		BookDTO BookDTO = bookService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(BookDTO));
	}

	/**
	 * DELETE /books/:id : delete the "id" Book.
	 *
	 * @param id the id of the BookDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/{id}")
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<Void> deleteBook(@PathVariable Long id) {
		log.debug("REST request to delete Book : {}", id);
		bookService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	
	/**
	 * GET /books : get all the books.
	 *
	 * @param pageable the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of books in body
	 */
	@GetMapping("/book-genres")
	public ResponseEntity<List<BookGenreDTO>> getAllBookGenres() {
		log.debug("REST request to get a list of all book genres");
		List<BookGenreDTO> bookGenreList = bookService.findAllBookGenres();
		return new ResponseEntity<>(bookGenreList, null, HttpStatus.OK);
	}

}
