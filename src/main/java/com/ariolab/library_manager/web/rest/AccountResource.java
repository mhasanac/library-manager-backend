package com.ariolab.library_manager.web.rest;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ariolab.library_manager.domain.User;
import com.ariolab.library_manager.security.jwt.JWTConfigurer;
import com.ariolab.library_manager.security.jwt.TokenProvider;
import com.ariolab.library_manager.service.UserService;
import com.ariolab.library_manager.service.dto.LoginVM;
import com.ariolab.library_manager.service.dto.UserDTO;
import com.ariolab.library_manager.service.mapper.UserMapper;
import com.ariolab.library_manager.web.util.HeaderUtil;

import javassist.NotFoundException;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

	private final Logger log = LoggerFactory.getLogger(AccountResource.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private TokenProvider tokenProvider;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private UserMapper userMapper;


	@PostMapping("/authenticate")
	public ResponseEntity<UserDTO> authorize(@Valid @RequestBody LoginVM loginVM) {
		log.debug("Authenticating user: "+loginVM.toString());
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

		Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		Date validityDate = tokenProvider.getValidityDate(true);
		String jwt = tokenProvider.createToken(authentication, validityDate);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
		httpHeaders.add(JWTConfigurer.TOKEN_VALIDITY_HEADER, String.valueOf(validityDate.getTime()));
		
		Optional<User> user = userService.getUserWithAuthorities();
		if(user.isPresent())
		{
			UserDTO userDTO = userMapper.userToUserDTO(user.get());
			return new ResponseEntity<>(userDTO, httpHeaders, HttpStatus.OK);
		}
		return null;
	}
	
	/**
	 * GET /account : get the current user.
	 *
	 * @return the current user
	 * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
	 */
	@GetMapping("/account")
	public ResponseEntity<UserDTO> getAccount() throws NotFoundException {
		Optional<UserDTO> account = userService.getUserWithAuthorities().map(UserDTO::new);
		if(account.isPresent()) {
			return ResponseEntity.ok().body(account.get());
		}
		return ResponseEntity.badRequest()
				.headers(HeaderUtil.createFailureAlert("Account", "idexists", "User could not be found")).body(null);
	}
}
