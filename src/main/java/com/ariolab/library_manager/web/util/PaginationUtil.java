package com.ariolab.library_manager.web.util;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;

/**
 * Utility class for handling pagination.
 */
public final class PaginationUtil {

	private PaginationUtil() {
	}

	public static <T> HttpHeaders generatePaginationHttpHeaders(
			Page<T> page,
			String baseUrl) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Total-Count", Long.toString(page.getTotalElements()));
		return headers;
	}
}
