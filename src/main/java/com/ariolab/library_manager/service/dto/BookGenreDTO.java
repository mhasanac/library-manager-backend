package com.ariolab.library_manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the BookGenre entity.
 */
public class BookGenreDTO implements Serializable {

	private static final long serialVersionUID = 6870727279537399552L;

	@NotNull
	private Long id;

	@NotNull
	@Size(max = 50)
	private String name;

	public BookGenreDTO(Long id, @NotNull @Size(max = 50) String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BookGenreDTO activityDTO = (BookGenreDTO) o;

		if (!Objects.equals(id, activityDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "BookGenreDTO [id=" + id + ", name=" + name + "]";
	}
}
