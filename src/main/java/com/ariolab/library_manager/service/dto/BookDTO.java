package com.ariolab.library_manager.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the Book entity.
 */
public class BookDTO implements Serializable {

	private static final long serialVersionUID = 6870727279537399552L;

	private Long id;

	@NotNull
	@Size(max = 50)
	private String name;
	
	private String description;
	
	@NotNull
	private Integer pageCount;
	
	private String author;
	
	private String publisher;
	
	@NotNull
	private BookGenreDTO bookGenre;
	
	private Instant createdDate;
	
	private Instant updatedDate;
	
	public BookDTO() {}
	
	public BookDTO(Long id, @NotNull @Size(max = 50) String name, String description, @NotNull Integer pageCount, String author, String publisher, 	@NotNull BookGenreDTO bookGenre, Instant createdDate, Instant updatedDate) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.pageCount = pageCount;
		this.author = author;
		this.publisher = publisher;
		this.bookGenre = bookGenre;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public BookGenreDTO getBookGenre() {
		return bookGenre;
	}
	public void setBookGenre(BookGenreDTO bookGenre) {
		this.bookGenre = bookGenre;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Instant updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BookDTO bookDTO = (BookDTO) o;

		if (!Objects.equals(id, bookDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}
	
	@Override
	public String toString() {
		return "BookDTO [id=" + id + ", name=" + name + ", description=" + description + ", pageCount=" + pageCount + ", author=" + author + ", publisher=" + publisher + ", bookGenre=" + bookGenre + ", createdDate="
				+ createdDate + ", updatedDate=" + updatedDate + "]";
	}
}
