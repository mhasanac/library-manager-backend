package com.ariolab.library_manager.service.mapper;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ariolab.library_manager.domain.Book;
import com.ariolab.library_manager.domain.BookGenre;
import com.ariolab.library_manager.service.dto.BookDTO;
import com.ariolab.library_manager.service.dto.BookGenreDTO;

/**
 * Mapper for the entity Book and its DTO BookDTO.
 */
@Service
public class BookMapper {


	public BookDTO entityToEntityDTO(Book entity) {
		if (entity == null) {
			return null;
		} else {
			Long id = entity.getId();
			String name = entity.getName();
			String description = entity.getDescription();
			String author = entity.getAuthor();
			String publisher = entity.getPublisher();
			Integer pageCount = entity.getPageCount();
			Instant createdDate = entity.getCreatedDate();
			Instant updatedDate = entity.getUpdatedDate();
			//empty activity
			BookGenreDTO bookGenre = null;
			//return entityDTO
			return new BookDTO(id, name, description, pageCount, author, publisher, bookGenre, createdDate, updatedDate);
		}
	}
	
	public Book entityDTOToEntity(BookDTO entityDTO) {
		if (entityDTO == null) {
			return null;
		} else {
			Long id = entityDTO.getId();
			String name = entityDTO.getName();
			String description = entityDTO.getDescription();
			String author = entityDTO.getAuthor();
			String publisher = entityDTO.getPublisher();
			Integer pageCount = entityDTO.getPageCount();
			Instant createdDate = entityDTO.getCreatedDate();
			Instant updatedDate = entityDTO.getUpdatedDate();
			
			//empty activity
			BookGenre bookGenre = null;

			//return entityDTO
			return new Book(id, name, description, pageCount, createdDate, updatedDate, bookGenre, author, publisher);
		}
	}

	public List<BookDTO> entityListToEntityDTOList(List<Book> entityList) {
		return entityList.stream().filter(Objects::nonNull).map(this::entityToEntityDTO).collect(Collectors.toList());
	}

	public List<Book> entityDTOListToEntityList(List<BookDTO> entityDTOList) {
		return entityDTOList.stream().filter(Objects::nonNull).map(this::entityDTOToEntity).collect(Collectors.toList());
	}
	
	public Book entityFromId(Long id) {
		if (id == null) {
			return null;
		}
		Book activity = new Book();
		activity.setId(id);
		return activity;
	}

}
