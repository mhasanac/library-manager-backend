package com.ariolab.library_manager.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ariolab.library_manager.domain.BookGenre;
import com.ariolab.library_manager.service.dto.BookGenreDTO;

/**
 * Mapper for the entity BookGenre and its DTO BookGenreDTO.
 */
@Service
public class BookGenreMapper {


	public BookGenreDTO entityToEntityDTO(BookGenre entity) {
		if (entity == null) {
			return null;
		} else {
			Long id = entity.getId();
			String name = entity.getName();
			//return entityDTO
			return new BookGenreDTO(
					id,
					name);
		}
	}
	
	public BookGenre entityDTOToEntity(BookGenreDTO entityDTO) {
		if (entityDTO == null) {
			return null;
		} else {
			Long id = entityDTO.getId();
			String name = entityDTO.getName();

			//return entityDTO
			return new BookGenre(
					id,
					name);
		}
	}

	public List<BookGenreDTO> entityListToEntityDTOList(List<BookGenre> entityList) {
		return entityList.stream().filter(Objects::nonNull).map(this::entityToEntityDTO).collect(Collectors.toList());
	}

	public List<BookGenre> entityDTOListToEntityList(List<BookGenreDTO> entityDTOList) {
		return entityDTOList.stream().filter(Objects::nonNull).map(this::entityDTOToEntity).collect(Collectors.toList());
	}
	
	public BookGenre entityFromId(Long id) {
		if (id == null) {
			return null;
		}
		BookGenre bookGenre = new BookGenre();
		bookGenre.setId(id);
		return bookGenre;
	}

}
