package com.ariolab.library_manager.service.mapper;

import org.springframework.stereotype.Service;

import com.ariolab.library_manager.domain.Authority;
import com.ariolab.library_manager.domain.User;
import com.ariolab.library_manager.service.dto.UserDTO;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity User and its DTO called UserDTO.
 *
 */
@Service
public class UserMapper {

	public UserDTO userToUserDTO(User user) {
		return new UserDTO(user);
	}

	public List<UserDTO> usersToUserDTOs(List<User> users) {
		return users.stream().filter(Objects::nonNull).map(this::userToUserDTO).collect(Collectors.toList());
	}

	public User userDTOToUser(UserDTO userDTO) {
		if (userDTO == null) {
			return null;
		} else {
			User user = new User();
			user.setId(userDTO.getId());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
			if (authorities != null) {
				user.setAuthorities(authorities);
			}
			return user;
		}
	}

	public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
		return userDTOs.stream().filter(Objects::nonNull).map(this::userDTOToUser).collect(Collectors.toList());
	}

	public User userFromId(Long id) {
		if (id == null) {
			return null;
		}
		User user = new User();
		user.setId(id);
		return user;
	}

	public Set<Authority> authoritiesFromStrings(Set<String> strings) {
		return strings.stream().map(string -> {
			Authority auth = new Authority();
			auth.setName(string);
			return auth;
		}).collect(Collectors.toSet());
	}
}
