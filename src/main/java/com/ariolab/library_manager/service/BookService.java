package com.ariolab.library_manager.service;

import com.ariolab.library_manager.domain.Book;
import com.ariolab.library_manager.domain.BookGenre;
import com.ariolab.library_manager.repository.BookGenreRepository;
import com.ariolab.library_manager.repository.BookRepository;
import com.ariolab.library_manager.service.BookService;
import com.ariolab.library_manager.service.dto.BookDTO;
import com.ariolab.library_manager.service.dto.BookGenreDTO;
import com.ariolab.library_manager.service.mapper.BookGenreMapper;
import com.ariolab.library_manager.service.mapper.BookMapper;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing Book.
 */
@Service
@Transactional
public class BookService {

	private final Logger log = LoggerFactory.getLogger(BookService.class);

	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private BookMapper bookMapper;
	
	@Autowired
	private BookGenreRepository bookGenreRepository;
	@Autowired
	private BookGenreMapper bookGenreMapper;

	/**
	 * Save a book.
	 *
	 * @param bookDTO the entity to save
	 * @return the persisted entity
	 */
	public BookDTO save(BookDTO bookDTO) {
		log.debug("Request to save Book : {}", bookDTO);
		Book book = bookMapper.entityDTOToEntity(bookDTO);
		book.setBookGenre(bookGenreMapper.entityFromId(bookDTO.getBookGenre().getId()));
		book = bookRepository.save(book);
		
		BookDTO result = findOne(book.getId());
		return result;
	}

	/**
	 * Get all the books.
	 * 
	 * @param pageable the pagination information
	 * @param bookGenreId 
	 * @param publisher 
	 * @param author 
	 * @param name 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<BookDTO> findAll(Pageable pageable, String name, String author, String publisher, Long bookGenreId) {
		log.debug("Request to get all Books");
		Page<Book> result = bookRepository.findAllByCustomQuery(pageable, name, author, publisher, bookGenreId);

		return result.map(book -> {
			BookDTO bookDTO = bookMapper.entityToEntityDTO(book);
			bookDTO.setBookGenre(bookGenreMapper.entityToEntityDTO(book.getBookGenre()));
			return bookDTO; 
		});
	}

	/**
	 * Get one book by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public BookDTO findOne(Long id) {
		log.debug("Request to get Book : {}", id);
		Book book = bookRepository.findOneWithEagerRelationships(id);
		BookDTO bookDTO = bookMapper.entityToEntityDTO(book);
		bookDTO.setBookGenre(bookGenreMapper.entityToEntityDTO(book.getBookGenre()));
		return bookDTO;
	}

	/**
	 * Delete the book by id.
	 *
	 * @param id the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Book : {}", id);

		Optional<Book> book = bookRepository.findById(id);
		if(book.isPresent()) {
			bookRepository.delete(book.get());
		}
	}
	
	/**
	 * Get all the book genres.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<BookGenreDTO> findAllBookGenres() {
		log.debug("Request to get all Book genres");
		List<BookGenre> result = bookGenreRepository.findAll();

		return bookGenreMapper.entityListToEntityDTOList(result);
	}
}
