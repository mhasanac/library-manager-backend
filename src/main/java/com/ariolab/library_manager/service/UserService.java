package com.ariolab.library_manager.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ariolab.library_manager.domain.Authority;
import com.ariolab.library_manager.domain.User;
import com.ariolab.library_manager.repository.AuthorityRepository;
import com.ariolab.library_manager.repository.UserRepository;
import com.ariolab.library_manager.security.SecurityUtils;
import com.ariolab.library_manager.service.dto.UserDTO;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {


	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final AuthorityRepository authorityRepository;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
	}
	
	public User createUser(UserDTO userDTO) {
		Optional<User> currentUser = getUserWithAuthorities();
		String createdBy = currentUser.get().getEmail();
		
		User user = new User();
		user.setCreatedBy(createdBy);
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		if (userDTO.getAuthorities() != null) {
			Set<Authority> authorities = userDTO.getAuthorities()
					.stream().map(authorityRepository::findById)
					.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
			user.setAuthorities(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
		user.setPassword(encryptedPassword);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}


	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		Optional<User> currentUser = getUserWithAuthorities();
		String modifiedBy = currentUser.get().getEmail();
		
		return Optional.of(userRepository.findById(userDTO.getId())).filter(Optional::isPresent).map(Optional::get).map(user -> {
			user.setLastModifiedBy(modifiedBy);
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			if(userDTO.getPassword() != null && !userDTO.getPassword().isEmpty()) {
				String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
				user.setPassword(encryptedPassword);
			}
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getAuthorities()
				.stream().map(authorityRepository::findById)
				.filter(Optional::isPresent).map(Optional::get).forEach(managedAuthorities::add);
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}

	public void deleteUser(String login) {
		userRepository.findOneByEmailIgnoreCase(login).ifPresent(user -> {
			userRepository.delete(user);
			log.debug("Deleted User: {}", user);
		});
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAll(pageable).map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByEmail(String email) {
		return userRepository.findOneWithAuthoritiesByEmail(email);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthorities(Long id) {
		return userRepository.findOneWithAuthoritiesById(id);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthorities() {
		return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByEmail);
	}

	/**
	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}
}
