package com.ariolab.library_manager.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "book")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Book implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(max = 50)
	@Column(name = "name", length = 50, nullable = false)
	private String name;

	@Column(name = "description")
	private String description;
	
	@Column(name = "page_count", nullable = false)
	private Integer pageCount;

	@Column(name = "created_date")
	private Instant createdDate;

	@Column(name = "updated_date")
	private Instant updatedDate;

	@NotNull
	@ManyToOne(optional = false)
	private BookGenre bookGenre;
	
	@NotNull
	@Size(max = 100) 
	@Column(name = "author", length = 100, nullable = true)
	private String author;
	
	@NotNull
	@Size(max = 100)
	@Column(name = "publisher", length = 100, nullable = true)
	private String publisher;
	
	public Book() {}

	public Book(Long id, @NotNull @Size(max = 50) String name, String description, Integer pageCount, Instant createdDate, Instant updatedDate, @NotNull BookGenre bookGenre, @NotNull @Size(max = 100) String author,
			@NotNull @Size(max = 100) String publisher) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.pageCount = pageCount;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.bookGenre = bookGenre;
		this.author = author;
		this.publisher = publisher;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Instant updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BookGenre getBookGenre() {
		return bookGenre;
	}
	public void setBookGenre(BookGenre bookGenre) {
		this.bookGenre = bookGenre;
	}

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Book book = (Book) o;
		if (book.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, book.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", description=" + description + ", pageCount=" + pageCount + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", bookGenre=" + bookGenre
				+ ", author=" + author + ", publisher=" + publisher + "]";
	}
}
