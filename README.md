Library manager - Java Backend
================================================

IDE Integration
---------------

### Import project

To import the project into Eclipse, follow these steps:
* Checkout from Git, e.g. into _C:\git\library_manager\library-manager-backend
* Create new workspace, e.g. _C:\java\workspaces\library-manager-backend
* Import project using _Maven > Existing Maven Project_ Wizzard

### Run

As a Spring Boot application, the Library manager backend can be easily ran with an embedded Tomcat by running the **com.ariolab.library_manager.LibraryManagerApp** class as a Java Application from within the IDE.


Build
-----

The build is based on Maven. Spring boot provides a convenient wrapper that can automatically download and execute maven. It will require JAVA_HOME to be set correctly though.


Swagger
----------

To access a swagger UI use following link http://localhost:8080/swagger-ui.html#/


Test
-----

User:
* Username:user@localhost
* Password:user

Administrator:
* Username:admin@localhost
* Password:admin


In order to test API's with each specific user please follow these steps:
1. Go to http://localhost:8080/swagger-ui.html#/
2. Open account-resource-> /api/authenticate 
3. Fill in data previously specified and click Execute
4. In response headers copy content of authorization header (e.g Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaGFzYW5hY0BnbWFpbC5jb20iLCJhdXRoIjoiIiwiZXhwIjoxNTQwNjY2NzM1fQ.4Qy8w_5kZMT4ylJSiqjz5nF6VUwIIBMl-p4NGVlFyHcn3pu8CjMM4Lv1MN0KWPrVIylAp7POXKODh_OzKLpOWw)
5. Click on authorize and paste copied authorization header value and click Authorize
6. You are able to test functionalities with logged in user
